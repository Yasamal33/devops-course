### Install and configure Postfix: sudo apt update

 - sudo hostnamectl set-hostname tr01-k8s-gl01 
 - sudo apt install mailutils

For ” General type of email configuration” window, select Internet site and click OK button 

 - sudo vi /etc/postfix/main.cf

Set Postfix to listen on the 127.0.0.1 loopback interface: 

    inet_interfaces = loopback-only myhostname=tr01-k8s-gl01

 - sudo systemctl restart postfix

To test sending e-mails:

 - echo "Postfix Send-Only Server" | mail -s "Postfix Testing" userx@example.com
 - cat /var/log/mail.log

### Gitlab (VM) installation:

- sudo apt update && sudo apt upgrade -y
- sudo apt install -y ca-certificates curl openssh-server locate
- curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
- cat /etc/apt/sources.list.d/gitlab_gitlab-ce.list
- sudo apt update && sudo apt -y install gitlab-ce
- sudo vim /etc/gitlab/gitlab.rb and add lines: external_url 'http://tr01-k8s-gl01.home.lab' - 
- sudo gitlab-ctl reconfigure

Use GUI (Settings -> CI/CD -> Runners), copy "Registration Token" (will be used to connect gitlab-runner)

Useful cmds:
- sudo gitlab-ctl reconfigure 
- sudo gitlab-rake gitlab:check 
- sudo gitlab-ctl status 
- sudo gitlab-ctl status
- sudo gitlab-ctl restart


### Enable HTTPS for Gitlab

- sudo cd /etc/gitlab/ ; openssl genrsa -aes128 -out server.key 2048
- sudo openssl rsa -in server.key -out server.key
- sudo openssl req -new -days 3650 -key server.key -out server.csr
- sudo openssl x509 -in server.csr -out server.crt -req -signkey server.key -days 3650
- sudo chmod 400 server.*

sudo vi /etc/gitlab/gitlab.rb and edit/add lines:

    external_url 'https://tr01-k8s-gl01.home.lab'
    nginx['enable'] = true 
    nginx['client_max_body_size'] = '250m'
    nginx['redirect_http_to_https'] = true 
    nginx['ssl_certificate'] = "/etc/gitlab/server.crt"
    nginx['ssl_certificate_key'] = "/etc/gitlab/server.key"
    nginx['ssl_protocols'] = "TLSv1.2 TLSv1.3"

- updatedb  
- locate gitlab.yml  sudo vi
- /var/opt/gitlab/gitlab-rails/etc/gitlab.yml

      host: tr01-k8s-gl01.home.lab
      port: 443
      https: true

- gitlab-rake gitlab:check

### Get SSL certificate and convert it to CRT (PEM) file (On one of K8S cluster's master node):

- openssl s_client -showcerts -connect tr01-k8s-gl01.home.lab:443 </dev/null 2>/dev/null | openssl x509 -outform PEM > tr01-k8s-gl01.home.lab.crt

- kubectl create ns gitlab-runner

Create secret for gitlab-runner (based on previously created crt key)
- kubectl -n gitlab-runner create secret generic tls2-secret --from-file=tr01-k8s-gl01.home.lab.crt

- helm repo add gitlab [https://charts.gitlab.io](https://charts.gitlab.io) 
- helm fetch gitlab/gitlab-runner 
- tar -xvf gitlab-runner-0.30.0.tgz 

Edit values.yaml:

`gitlabUrl: https://tr01-k8s-gl01.home.lab/`

`certsSecretName: tls2-secret`

`concurrent: 1`

`checkInterval: 10`

`rbac: create: true`

And start helm install:

- helm install --namespace gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner

### The result

![Runner](runner.png?raw=true "Gitlab Runner")
