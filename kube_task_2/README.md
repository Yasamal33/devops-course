 # Configure docker on all nodes intended to cluster 

kubectl get pods -n nexus -o wide

1. Note NODE IP that hosts NEXUS POD and put it in docker daemon file.
Create (if not exists) sudo vi /etc/docker/daemon.json with following content

{
        "insecure-registries": ["10.233.41.107:8183"],
        "registry-mirrors": ["http://10.233.41.107:8183"]
}


2. systemctl restart docker.service

sudo docker info | tail -n 7

and check the end of info output for following lines:

Insecure Registries:
 10.233.41.107:8181
 127.0.0.0/8
Registry Mirrors:
 http://10.233.41.107:8181/
Live Restore Enabled: false




# Configure kubectl to get images from private repository to be done on all nodes                                 




Copy docker daemon config to kubelet folder
sudo cp ~/.docker/config.json /var/lib/kubelet/config.json






#   Create namespace named nexus:        


kubectl create ns nexus

OR

kubectl apply -f 1_create_namespace_nexus.yaml





# Generate deploymet manifest file.                                 
# Use nexus/sonatype image for this deployment. set the replicas to 1.   


kubectl create deployment nexus --image=sonatype/nexus3 -n nexus --replicas=1 --dry-run=client -o wide > 2_create_deployment_nexus.yaml



# Create this deployment in nexus namespace 


kubectl create -f 2_create_deployment_nexus.yaml

or

kubectl create -n nexus -f 2_create_deployment_nexus.yaml (if ns name is not defined in manifest)




# Create NodePort type service for this deployment   


kubectl apply -f manifests/3_create_svc_np_nexus_management.yaml




# After that login to sonatype and create Docker proxy and Docker hosted type repository with anonimous access.         

1. Get Nexus initial password

kubectl exec -i -n nexus nexus_pod_name cat nexus-data/admin.password

2. Create repositories

   name: docker-proxy-pull-8183
   http: 8183
   Allow anonymous docker pull ( Docker Bearer Token Realm required )
   Allow clients to use the V1 API to interact with this repository
   Remote storage: https://registry-1.docker.io
   Docker index: Docker Hub

   name: docker-hosted-push-8182
   http: 8182
   Allow anonymous docker pull ( Docker Bearer Token Realm required )
   Allow clients to use the V1 API to interact with this repository

   Optional (if it's necessary to use same port for push & pull)
   Create REPO
   name: docker-group
   http: 8181
   Member repositories: docker-proxy

3. Anonymous access settings:
    Security -> Anonymous access -> Enable "Allow anonymous users to access the server" / Local Authorizing Realm
    Security -> Realms -> move "Docker Bearer Token Realm" to Active
    Security -> Users -> anonymous -> Roles -> Grant "nx-admin" (if you want to enable anonymous image push)



#   For this repositories you must create ClusterIp service.  Create ONE clusterip service for both repository!!!       
                                                             


kubectl create -f manifests/4_create_svc_cip_nexus_management.yaml



# Try create nginx pod using image over sonatype.        


kubectl create deployment private-repo-pull-test -n nexus --image=10.233.41.107:8183/nginx --replicas=3
kubectl delete deploy -n nexus private-repo-pull-test



# Now expose docker over tcp in one Node                 


1. On the node to be exposed:
vi /etc/systemd/system/docker.service.d/override.conf

and add lines:

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock

systemctl restart docker.service
systemctl daemon-reload



#  Create pod from image docker. Get into pod and set Docker_host. 
#  After that try get image list,pull image from sonatype          
#  docker hosted repository and create docker image.               


kubectl apply -f 5_Last_step.yaml




#                Testing                     


kubectl run docker-test --image=10.233.41.107:8183/docker --env="DOCKER_HOST=192.168.10.225" -n nexus docker images
kubectl logs -n nexus docker-test
kubectl delete pod -n nexus docker-test
sudo docker run --privileged -e DOCKER_HOST=192.168.10.223 -it 10.233.41.107:8183/docker sh




Little "tool" for those who read (or scrolled) until the end.

watch -n 1 'printf \\nDeployments\\n && kubectl get deploy -n nexus && printf \\nReplicaSets\\n && kubectl get rs -n nexus && printf \\nPODs\\n && kubectl get pods -n nexus && printf \\nServices\\n && kubectl get svc -n nexus'

