#!/bin/bash

#===========================================================================================#
#               Script for Lesson 3.                                                        #
#               Task: Find correct keys in remote project                                   #
#               Project URL: https://gitlab.com/ingress.tasks/git-script/-/tree/master      #
#               Task author/DevOps course trainer: Emin B. https://gitlab.com/ingress.tasks #
#                                                                                           #
#               Script author: Orkhan S.https://gitlab.com/Yasamal33                        #
#                                                                                           #
#                                                                                           #
#               Script ver.: 1.0                                                            #
#               Developed and tested on: "Ubuntu 20.04.2 LTS"                               #
#                                                                                           #
# All the comments that conttains ONLY bash/linux commands are functional and can be        #
# uncommented to visually track the execution of the script.                                #
# A some variables are hardcoded. But also can be easily re-defined                         #
#                                                                                           #
#                                                                                           #
# The main goal during creating this script was using most of learned commands within       #
# lesson 3, related to commonly using bash commands                                         #
#                                                                                           #
# Useful information:                                                                       #
# RSA key masks contains:Status of SSH connection test [OK|NOK] + branch name + commit hash #
#                                                                                           #
# Haven't implemented:                                                                      #
# * Consistency check user inputs                                                           #
# * Automated selecting correct RSA file and performing push in this way                    #
# * README.md for gitlab                                                                    #
# * CASE statements                                                                         #
#                                                                                           #
#===========================================================================================#

#++++++++++
#Defining global variables

IFS=$'\n'
RSA_filename_mask=emin_gitlab_rsa

master_folder=git-script
#printf "\nChanging folder..."
#sleep 1


#Defining remote project URL
printf "Enter HTTP or SSH URL of the project to clone it.\nDefault: https://gitlab.com/ingress.tasks/git-script.git\n"
printf "Leave it blank if you want to use defaults.\n"
read -p "URL:" PROJECT_URL
if [[ $PROJECT_URL == '' ]]; then

  echo "Default selected"
  #echo $PROJECT_FOLDER
  PROJECT_URL=https://gitlab.com/ingress.tasks/git-script.git

fi

#Defining project folder using user input
printf "Enter the FULL PATH of folder which will contain "git-script" project.\n"
printf "Example: /home/orxan/task55 \n"
printf "Leave it blank if you want to use defaults.\n"
printf "Or type . (dot) to use actual folder.\n"
printf "default: ~/Desktop/Task3\n"

read -p "Local folder path:" FOLDER_NAME

if [[ $FOLDER_NAME == '' ]]; then

  echo "Default selected"
  #echo $PROJECT_FOLDER
  PROJECT_FOLDER=~/Desktop/Task3

elif [[ $PROJECT_FOLDER == '.' ]]; then

  echo "Actual folder selected"
  PROJECT_FOLDER=`pwd`

else

  PROJECT_FOLDER=$FOLDER_NAME
  echo "Local folder for the project is $PROJECT_FOLDER"
  read -p  "press key"

fi


#echo $PROJECT_FOLDER

#Globals defined
#++++++++++




#++++++++++
#Cheking folder condition

#Prepare git folder and create 2 branches
#to avoid checking other trainees branches parsing
if ! [[ -d $PROJECT_FOLDER ]]; then
  mkdir $PROJECT_FOLDER
  cd $PROJECT_FOLDER
  git clone $PROJECT_URL
  cd $master_folder
  git checkout branch1 2> /dev/null
  git checkout master 2> /dev/null
  git checkout branch2 2> /dev/null
  git checkout master 2> /dev/null
  git remote set-url origin git@gitlab.com:ingress.tasks/git-script.git


else

  cd $PROJECT_FOLDER/$master_folder
  #printf "\nNow we are in: $master_folder"
  #sleep 1
  #printf "\nResetting branch to Master"

  git checkout master

fi

#End of folder condition check
#++++++++++


#Define variable contains list of available branches in project
branches_list=`git branch | sed 's/..//' | sort -r`

  #sleep 1

  #Uncomment lines 86-97 if you want to see available branches to be processed

  #printf "\nBranches available in the project:\n$branches_list"
  #sleep 1
  #printf "\n"
  #echo `git checkout master`
  #printf "\n"
  #echo `git branch`
  #sleep 1


#Simple "copy files by mask" function
function copy_found_keys () {
  cd ~/.ssh/
    mkdir -p ssh_found_keys/OK ssh_found_keys/NOK
    mv OK* ssh_found_keys/OK && mv NOK* ssh_found_keys/NOK
}


#Final push function.
#Creates a new branch and configure it
#Collects all found keys from ssh folder, create separate folders for both kind of found keys (OK/NOK)
#Moves them to the new branch folder by following commit and push to remote
function collect_and_push () {
  cd $PROJECT_FOLDER/git-script
  echo `pwd`
  git checkout -b branch_orkhan
  git config --global bimbo.jones@mail.ru
  git config --global user.name "Orkhan Sultanov"
  mkdir found_correct_keys found_incorrect_keys 2> /dev/null
  echo "Project folder is $PROJECT_FOLDER"
  cp -r ~/.ssh/ssh_found_keys/OK/* $PROJECT_FOLDER/git-script/found_correct_keys/
  cp -r ~/.ssh/ssh_found_keys/NOK/* $PROJECT_FOLDER/git-script/found_incorrect_keys/
  git add .
  git commit -m "Create and place found keys to relevant folders"
  #Dry run of git push to imitate real push and review the results.
  #Also can be used for debugging and to respect to project maintainer.
  git push --set-upstream origin branch_orkhan --dry-run
  #put correct key to ~/.ssh/, rename it to id_rsa
  #git push --set-upstream origin branch_orkhan

}




#Main loop
#Used nested loop trick
#First to switch between branches
#Second to switch between every commit of branch, selected using first loop
for branch_name in $branches_list; do

  #echo "Start processing for $branch_name"

  #sleep 1

    for commit_hash in `git log --decorate=no | grep "commit " | awk 'BEGIN{FS=" "}{print $2}'`; do

      RSA_file=$RSA_filename_mask-$branch_name-$commit_hash

      #sleep 1

      #echo "Branch is:$branch_name. Commit to be processed:$commit_hash"

      #sleep 1

      git checkout $commit_hash 2> /dev/null

      #sleep 1

      #printf "\nSwitched to the next commit \n"

      #printf "\nCopying id_rsa file to ssh folder\n"

      cp id_rsa ~/.ssh/$RSA_file

      chmod 600 ~/.ssh/$RSA_file

      #printf "File $RSA_file has been copied to ~/.ssh/ folder with 600 rights \n "

      #read -p "Press any key to continue..."

      #printf "\n Trying to connect to Gitlab with new key \n $RSA_file \n"

      #This IF statements runs ssh command with the key files created in advance
      #Check them on every loop and depending on result of check rename them
      if [ `ssh -o "IdentitiesOnly=yes" -i "~/.ssh/$RSA_file" -Tq git@gitlab.com` ]; then

        mv -f ~/.ssh/$RSA_file ~/.ssh/OK-$RSA_file

      else

        mv -f ~/.ssh/$RSA_file ~/.ssh/NOK-$RSA_file

      fi

    done #For nested FOR loop


done #For parent FOR loop

#Performing housekeeping using pre-defined functions

ls ~/.ssh/ | egrep *OK > /dev/null

if [ $? == 0 ]; then

  copy_found_keys

fi

collect_and_push
