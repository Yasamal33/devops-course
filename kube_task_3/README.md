1. NFS part


Server:

sudo apt update && sudo apt install nfs-kernel-server -y
sudo mkdir -p /mnt/nfs_share
sudo mkdir -p /mnt/nfs_share
sudo chmod 777 /mnt/nfs_share/
sudo vim /etc/exports
/mnt/nfs_share  *(rw,sync,no_subtree_check)
sudo exportfs -a
sudo systemctl restart nfs-kernel-server


Client:


sudo apt update && sudo apt install nfs-common -y
sudo mkdir /mnt/nfs_folder
sudo mount 192.168.10.224:/mnt/nfs_share /mnt/nfs_folder/
sudo vi /etc/fstab
192.168.10.224:/mnt/nfs_share /mnt/nfs_folder	nfs	defaults	0	0
sudo mount -a
sudo reboot


2. kubectl create ns nexus
3. kubectl create -n nexus -f nfs_add.yaml
4. kubectl label nodes tr01-k8s-w01 node=gold
5. kubectl apply -f nfs.yaml



watch -n 1 'printf \\nDeployments\\n && kubectl get deploy -n nexus && printf \\nReplicaSets\\n && kubectl get rs -n nexus && printf \\nPODs\\n && kubectl get pods -n nexus && printf \\nServices\\n && kubectl get svc -n nexus && printf \\nPV\\n && kubectl get persistentvolume -n nexus && printf \\nPVC\\n && kubectl get persistentvolumeclaims -n nexus'

# The result

![The result of applied commands](result.png?raw=true "The result")
