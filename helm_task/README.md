Install helm
wget https://get.helm.sh/helm-v3.6.1-linux-amd64.tar.gz
tar -zxvf helm-v3.6.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm


Folder name must match with "name" variable's value in Chart.yaml

Install chart
helm install "chart name" .
  helm install nginx-demo .

List deployed charts
helm list


Upgrade existing chart
helm upgrade "chart name" .
  helm upgrade nginx-demo .

Upgrade existing chart using file
helm upgrade "chart name" . -f "filename"
  helm upgrade nginx-demo . -f values.yaml

Rolling back the changes/to the previous helm chart version
helm rollback "chart name"
  helm rollback nginx-demo

Install plugin to activate push in helm


Metallb installation (using chart)
helm repo add stable https://charts.helm.sh/stable
helm repo update
helm fetch stable/metallb
tar -xvf metallb-0.12.1.tgz


Create NS for metallb (if it's not presented in configmap file)
kubectl create ns metal

Apply configmap (changing IP address range in advance)
kubectl apply -f ../metallb-config.yaml -n metal
helm install metallb . -f values.yaml -n metal

To check manually create deploy and service
kubectl crteate service loadbalancer "lb name" --protocol=src_port:dst_port
kubectl create service loadbalancer my-lbs --tcp=80:80 -n metal
kubectl get svc -n metal

or

kubectl apply -f test_deploy.yaml



NFS Provisioner installation
helm install nfs-provisioner . -f values.yaml

Chartmuseum
kubectl create -f storage.yaml
kubectl create -f deployment.yaml


Helm push plugin installation:
helm plugin install https://github.com/chartmuseum/helm-push.git

After successful deployment and tests pack the helm chart:
helm package .
helm repo add chartmuseum http://10.233.66.29:8080
helm push nfs-provisioner/ chartmuseum


![nfs-provisioner](nfs-provisioner.png?raw=true "nfs-provisioner")
![chartmuseum.png](chartmuseum.png?raw=true "chartmuseum.png")
