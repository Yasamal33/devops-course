#!/bin/bash
#Prerequisites:
#Running and configured Sonatype Nexus


#Define global variables
CI_folder_on_host=/home/orxan/ci
CI_folder_in_container=/ci

#Run container to clone the repo to mounted folder
docker run -v $CI_folder_on_host/:$CI_folder_in_container alpine sh -c "apk add git;git clone https://gitlab.com/bayramzadehes/flask-demo.git $CI_folder_in_container/"

#Check availability of cloned repo
if [[ -d $CI_folder_on_host/.git ]]; then
  echo "Repository successfuly cloned"

else
  echo "Please clone repository again"

fi


#Create docker file
echo "Creating Dockerfle"
echo

cat << EOF > $CI_folder_on_host/Dockerfile
FROM alpine:3.5
RUN apk add --update py2-pip
RUN pip install --upgrade pip
WORKDIR $CI_folder_in_container
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt
COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/
EXPOSE 5000
CMD python /usr/src/app/app.py
EOF

#Check for Dockerfile availability
if [[ -f $CI_folder_on_host/Dockerfile ]]; then
  echo "Dockerfile created"

else
  echo "Please check $CI_folder_on_host contents manually"

fi

#Task specific variables

#Defining docker private repo
DOCKER_PRIVATE_REPO=192.168.10.225:8089

#Define application name
APP_NAME='orkhan_app'

#Define image tag
IMAGE_TAG=`git -C $CI_folder_on_host/ log --pretty=format:"%h"`

#Define image name with commit id
DOCKER_IMAGE_NAME=$DOCKER_PRIVATE_REPO/$APP_NAME:$IMAGE_TAG

#Docker image build and push to repository
docker run -e DOCKER_HOST=192.168.10.225 -v $CI_folder_on_host:/$CI_folder_in_container -v /home/orxan/.docker/config.json:/root/.docker/config.json -w '/ci' docker sh -c "docker build . --tag $DOCKER_IMAGE_NAME;docker push $DOCKER_IMAGE_NAME"

#Start container and
docker run -p 5000:5000 --name $APP_NAME -d $DOCKER_IMAGE_NAME

#perform content and functionality tests:


read -t 6 -p "Checking app status..."
echo

app_status=`curl -Is 192.168.10.225:5000 | head -n 1`

if [[ $? == 0 ]]; then

  echo "App successfuly deployed"

else
  echo "Something went wrong. Please check container/image"
fi

